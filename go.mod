module gitee.com/guoyucode/logs

require (
	github.com/belogik/goes v0.0.0-20151229125003-e54d722c3aff
	github.com/cloudflare/golz4 v0.0.0-20150217214814-ef862a3cdc58
	github.com/gogo/protobuf v1.1.1
	github.com/kr/pretty v0.1.0 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
