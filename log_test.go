package logs

import (
	"testing"
	"time"
)

func Test_log(t *testing.T) {
	Async(100)
	for ;true; {
		time.Sleep(time.Second)
		Info("写入测试日志, %v秒", time.Time{}.Unix())
		Debug("写入测试日志,debug级别日志不会写入到文件, %v秒", time.Time{}.Unix())
	}
}
